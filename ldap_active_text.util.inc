<?php

/*
 * [INTERNAL] Retorna o hash de configuracao da query passada como parametro
 *            (ou a configuracao de todas as queries, caso o parametro seja
 *            nulo)
 */
function ldap_active_text_get_query($query_name = '') {
	$query_config = variable_get('ldap_active_text_config', array());
	$result = NULL;

	if($query_name == '')
		// Retorna a configuracao de todas as queries
		$result = $query_config;

	if(isset($query_config[$query_name]))
		$result = $query_config[$query_name];

	return $result;
}


/*
 * [INTERNAL] Intepreta o corpo do texto ativo, substituindo
 *            os parametros pelos respectivos valores
 */
function ldap_active_text_parse($query_name, $keyword = '_DEFAULT_') {
	$q = ldap_active_text_get_query($query_name);
	$multiple = $q['multiple_values'];

	if($q['fixed_input'])
		$keyword = module_exists('token') ? token_replace($q['input_parameter']) : $q['input_parameter'];

	$data_set = ldap_active_text_evaluate_query($keyword, $query_name, $multiple);

	if(empty($data_set))
		return $q['display_empty'];

	$text = '';
	foreach($data_set as $data) {
		$line = $q['display_body'];
		$empty_line = TRUE;
		while(preg_match('/\{([a-z0-9_\.:]+)\}/i', $line, $matches, PREG_OFFSET_CAPTURE)) {
			$offset = $matches[0][1];
			$length = strlen($matches[0][0]);
			$token = strtolower($matches[1][0]); // Token contem nomes de parametros e de active texts: deve ser minusculo
			$replace_str = '';
			switch(true) {
				case strpos($token, '.') !== FALSE:
					$parameters = explode('.', $token);
					switch($parameters[1]) {
						case 'jpg': $replace_str = $data[$parameters[0]] ? 'data:image/jpeg;base64,' . base64_encode($data[$parameters[0]]) : ''; break;
					}
				break;
				case strpos($token, ':') !== FALSE:
					$parameters = explode(':', $token);
					$replace_str = ldap_active_text_parse($parameters[1], $data[$parameters[0]]);
				break;
				default:
					$replace_str = $data[$token];
			}
			$line = substr_replace($line, $replace_str, $offset, $length);
			$empty_line = $empty_line && empty($replace_str);
		}
		// Se todos os parametros da linha forem nulos, usa $q['display_empty']
		$text .= $empty_line ? $q['display_empty'] : $line;
	}
	return $q['display_prefix'] . $text . $q['display_suffix'];
}


/*
 * [INTERNAL] Retorna o valor da query $query_name para o atributo de entrada $keyword
 */
function ldap_active_text_evaluate_query($keyword, $query_name, $multiple = FALSE) {
	$query_config = ldap_active_text_get_query($query_name);
	$query_string = $query_config['query_string'];
	$cache_time   = $query_config['cache_time'];
	$result = NULL;

	if(empty($query_string))
		// Query nao existe
		return NULL;

	$cid = 'query:' . $query_name . ':' . $keyword;
	if($cache = cache_get($cid, 'cache_ldap_active_text')) {
		$data = $cache->data;

		if(isset($data))
			// Resultado encontrado no cache
			return $data;
	}

	if(preg_match_all('/\{([a-z0-9]+:[a-z0-9]+:[a-z0-9,]+)\}/i', $query_string, $matches)) {
		$queries = $matches[1];
		$last_query_index = count($queries) - 1;
		for($i = 0; $i <= $last_query_index; $i++) {
			$query_parameters = explode(':', $queries[$i]);
			$output_attributes = explode(',', $query_parameters[2]);
			if($i == $last_query_index) {
				// Apenas a ultima query pode retornar multiplos valores para o(s) atributo(s) de saida
				$sorting_attribute = strtolower($output_attributes[0]);
				$result = ldap_active_text_ldap_query($keyword, $query_parameters[0], $query_parameters[1], $output_attributes, $multiple);
				if(empty($result))
					return NULL;
			}else {
				$result = ldap_active_text_ldap_query($keyword, $query_parameters[0], $query_parameters[1], $output_attributes);
				if(empty($result))
					return NULL;
				// As queries intermediarias nao suportam multiplos atributos de saida
				$keyword =  $result[0][strtolower($query_parameters[2])];
			}
		}
	}

	if($multiple)
		// Ordena os resultados
		usort($result, function($a, $b) use ($sorting_attribute) {
			return strnatcmp($a[$sorting_attribute], $b[$sorting_attribute]);
		});

	// Guarda o resultado no cache
	cache_set($cid, $result, 'cache_ldap_active_text', REQUEST_TIME + 60 * $cache_time);

	return $result;
}


/*
 * [INTERNAL] Executa a query LDAP "$ldap_query_name" com o filtro "($keyword=$input_parameter)"
 *            e retorna os valores dos atributos especificados no array "$output_attributes"
 */
function ldap_active_text_ldap_query($keyword, $input_parameter, $ldap_query_name, $output_attributes, $multiple = FALSE) {
	$filter = strtolower("($input_parameter=$keyword)");
	$query_results = '';

	if($query = ldap_query_get_queries($ldap_query_name, 'enabled', TRUE, TRUE)) {
		if(strtolower($input_parameter) == 'dn') {
			// Busca por DN: buscar pela base e nao pelo filtro
			$query->baseDn = array($keyword);
			$query->scope = LDAP_SCOPE_BASE;
		} else
			$query->filter = '(&' . $query->filter . $filter . ')';
		$query->sizelimit = $multiple ? 0 : 1;
		$query_results = $query->query();
	}

	if(empty($query_results) || ! $query_results['count'])
		return NULL;

	$result_count = $multiple ? $query_results['count'] : 1;

	// Retorna um array com os atributos, na forma atributo => valor
	$results = array();
	for($i = 0; $i < $result_count; $i++) {
		$result = array();
		foreach($output_attributes as $output_attribute) {
			$output_attribute = strtolower($output_attribute);
			$result[$output_attribute] = $query_results[$i][$output_attribute][0];
		}
		$result['dn'] = $query_results[$i]['dn'];
		$results[] = $result;
	}

	return $results;
}
