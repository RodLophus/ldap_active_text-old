<?php

/*
 * Pagina inicial da configuracao, com a lista de queries
 */
function ldap_active_text_query_list() {
	module_load_include('inc', 'ldap_active_text', 'ldap_active_text.util');
	global $base_url;
	$config_url = $base_url . '/admin/config/content/ldap_active_text/queries';
	$query_config = ldap_active_text_get_query();

	$rows = array();
	foreach($query_config as $name => $cfg) {
		$rows[] = array(
			$name, 
			$cfg['description'], 
			($cfg['multiple_values'] ? 'List' : 'Single-value') . ', ' . ($cfg['fixed_input'] ? 'Static' : 'Contextual'),
			$cfg['query_string'], 
			$cfg['enabled'] ? 'Yes' : 'No',
			"<a href=\"$config_url/edit/$name\">Edit</a> | ".
				"<a href=\"$config_url/delete/$name\">Delete</a> | ".
				"<a href=\"$config_url/test/$name\">Test</a>"
		);
	}

	$output = theme('table', array(
		'header' => array('Name', 'Description', 'Type', 'Query String', 'Enabled', 'Actions'),
		'rows' => $rows,
	));

	// Formulario de limpeza de cache
	$form = drupal_get_form('ldap_active_text_cache_form');
	$output .= render($form);

	 return $output;
}


/*
 * Formulario de limpeza de cache
 */
function ldap_active_text_cache_form($form, $form_state) {
	$form = array();

	$form['ldap_active_text_cache'] = array(
		'#type' => 'fieldset',
		'#title' => t('Query Cache Control'),
	);

	$form['ldap_active_text_cache']['ldap_active_text_cache_flush'] = array(
		'#type' => 'submit',
		'#value' => t('Clear all queries cache'),
	);
	
	return $form;
}


/*
 * Formulario de criacao / edicao / remocao de queries
 */
function ldap_active_text_config_form($form, $form_state, $mode = 'add', $query_name = NULL) {
	module_load_include('inc', 'ldap_active_text', 'ldap_active_text.util');
	global $base_url;
	$config_url = $base_url . '/admin/config/content/ldap_active_text/queries';
	$help_url = $base_url . '/admin/help/ldap_active_text';

	$name_tag = array(
		'add' => t('Add Active Text'),
		'edit' => t('Update Active Text'),
		'delete' => t('Delete Active Text')
	);

	// Default values
	$enabled = TRUE;
	$name = $description = $query_string = $input_parameter = '';
	$fixed_input = $multiple_values = FALSE;
	$display_prefix = $display_body = $display_suffix = $display_empty = '';
	$cache_time = 720;

	if($mode == 'edit' || $mode == 'delete') {
		// Usa como default os valores gravados
		$q = ldap_active_text_get_query($query_name);
		$name = $query_name;
		$enabled         = $q['enabled'];
		$description     = $q['description'];
		$query_string    = $q['query_string'];
		$fixed_input     = $q['fixed_input'];
		$input_parameter = $q['input_parameter'];
		$cache_time      = $q['cache_time'];
		$display_prefix  = $q['display_prefix'];
		$display_body    = $q['display_body'];
		$display_suffix  = $q['display_suffix'];
		$display_empty   = $q['display_empty'];
		$multiple_values = $q['multiple_values'];
	}

	$form = array();

	$form['#lac_mode'] = $mode;
	
	$form['ldap_active_text_form_title'] = array(
		'#type' => 'markup',
		'#markup' => '<h1>' . $name_tag[$mode] . ($query_name ? t(' (query: %query_name)', array('%query_name' => $query_name)) : '' ) . '</h1>'
	);

	$form['ldap_active_text_query'] = array(
		'#type' => 'fieldset',
		'#title' => t('Query Definition'),
		'#disabled' => $mode == 'delete'
	);

	$form['ldap_active_text_query']['ldap_active_text_query_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enabled'),
		'#default_value' => $enabled
	);

	$form['ldap_active_text_query']['ldap_active_text_query_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Query name'),
		'#disabled' => $mode != 'add',
		'#required' => TRUE,
		'#default_value' => $name,
		'#description' => t('The Active Text query name must be unique and contain only lowercase letters, numbers, and underscores.')
	);

	$form['ldap_active_text_query']['ldap_active_text_query_description'] = array(
		'#type' => 'textfield',
		'#title' => t('Description'),
		'#default_value' => $description,
		'#description' => t('A text to describe the query.')
	);

	$form['ldap_active_text_query']['ldap_active_text_query_string'] = array(
		'#type' => 'textfield',
		'#title' => t('Query string'),
		'#size' => '100',
		'#required' => TRUE,
		'#default_value' => $query_string,
		'#description' => t('Format: {input_parameter:ldap_query_name:output_parameter} ... {input_parameter:ldap_query_name:parameter,parameter,...}.<br>' .
			'Please see the <a href="@help_page" target="_blank">module\'s help section</a> for details and examples.',
			array('@help_page' => $help_url . '#query_string'))
	);

	$form['ldap_active_text_query']['ldap_active_text_query_fixed_input_parameter'] = array(
		'#type' => 'checkbox',
		'#title' => t('Use a fixed input parameter (static query)'),
		'#default_value' => $fixed_input,
		'#description' => t('Check to inform a fixed input parameter for this query (it will ignore parameters passed via the "command line").')
	);

	$form['ldap_active_text_query']['ldap_active_text_query_input_parameter'] = array(
		'#type' => 'textfield',
		'#title' => t('Input parameter'),
		'#default_value' => $input_parameter,
		'#states' => array(
			'visible' => array(
				':input[name="ldap_active_text_query_fixed_input_parameter"]' => array('checked' => TRUE),
			)
		),
		'#description' => t('The fixed input parameter to use on this query. You can use Drupal tokens here if the Token module is enabled.')
	);

	$form['ldap_active_text_query']['ldap_active_text_cache_time'] = array(
		'#type' => 'textfield',
		'#title' => t('Cache time'),
		'#size' => '10',
		'#required' => TRUE,
		'#default_value' => $cache_time,
		'#description' => t('How long will the results of this query be kept on cache (time in minutes).<br>' .
			'Note: this query\'s cache will be cleaned every time it is saved.')
	);

	$form['ldap_active_text_presentation'] = array(
		'#type' => 'fieldset',
		'#title' => t('Data Presentation'),
		'#disabled' => $mode == 'delete',
		'#description' => t('The following configuration parameters may have any HTML tags allowed by your input method definition.')
	);

	$form['ldap_active_text_presentation']['ldap_active_text_display_prefix'] = array(
		'#type' => 'textfield',
		'#title' => t('Prefix'),
		'#size' => '100',
		'#default_value' => $display_prefix,
		'#description' => t('An optional prefix, to be included before the result of the query.')
	);

	$form['ldap_active_text_presentation']['ldap_active_text_display_body'] = array(
		'#type' => 'textfield',
		'#title' => t('Body'),
		'#size' => '100',
		'#required' => TRUE,
		'#default_value' => $display_body,
		'#description' => t('Use the tags "{parameter}" and "{parameter:active_text_query}" to inform where the results of the query string are going to be placed.<br>' .
			'Please see the <a href="@help_page" target="_blank">module\'s help section</a> for details and examples.',
			array('@help_page' => $help_url . '#data_presentation'))
	);

	$form['ldap_active_text_presentation']['ldap_active_text_display_suffix'] = array(
		'#type' => 'textfield',
		'#title' => t('Suffix'),
		'#size' => '100',
		'#default_value' => $display_suffix,
		'#description' => t('An optional suffix, to be included after the result of the query.')
	);

	$form['ldap_active_text_presentation']['ldap_active_text_display_empty'] = array(
		'#type' => 'textfield',
		'#title' => t('Empty-result content'),
		'#description' => t('To be displayed instead of prefix+body+suffix when the query string returns no results.'),
		'#size' => '100',
		'#default_value' => $display_empty
	);

	$form['ldap_active_text_presentation']['ldap_active_text_display_multiple'] = array(
		'#type' => 'checkbox',
		'#title' => t('Multiple values'),
		'#default_value' => $multiple_values,
		'#description' => t('Check this to make your query return a list of values. The query\'s body (specified above) will be repeated once per result found.')
	);

	$form['ldap_active_text_submit'] = array(
		'#type' => 'submit',
		'#name' => 'save',
		'#value' => $name_tag[$mode],
	);

	$form['ldap_active_text_cancel'] = array(
		'#type' => 'markup',
		'#markup' => '<input id="edit-ldap-active-text-test-cancel" class="form-submit" type="button" value="' . t('Cancel') .'" onclick="window.location.href=\'' . $config_url . '\'"/>',
	);

	$form['#theme'] = 'system_settings_form';

	// Nao usamos system_settings_form() aqui porque vamos gravar os dados
	// em um hash personalizado, e nao em variaveis individuais
	return $form;
}


/*
 * Formulario de teste de queries
 */
function ldap_active_text_test_form($form, &$form_state, $query_name = NULL) {
	module_load_include('inc', 'ldap_active_text', 'ldap_active_text.util');
	global $base_url;
	$config_url = $base_url . '/admin/config/content/ldap_active_text/queries';

	$query_config = ldap_active_text_get_query($query_name);
	$fixed_input = $query_config['fixed_input'];
	$input_parameter = $query_config['input_parameter'];

	if(isset($form_state['values']))
		$values = $form_state['values'];

	$form  = array();

	$form['ldap_active_text_test_header'] = array(
		'#type' => 'markup',
		'#markup' => '<h1>' . t('Test Active Text (query: %query_name)', array('%query_name' => $query_name)) . '</h1>',
	);

	$form['ldap_active_text_test_parameter'] = array(
		'#type' => 'textfield',
		'#title' => t('Parameter'),
		'#default_value' => $input_parameter,
		'#disabled' => $fixed_input,
		'#required' => TRUE
	);

	$form['ldap_active_text_test_results_block'] = array(
		'#type' => 'fieldset',
		'#title' => t('Results')
	);

	$form['ldap_active_text_test_results_block']['ldap_active_text_test_results'] = array(
		'#type' => 'markup',
		'#markup' => t('(click on the <em>Test</em> button to test the query)'),
		'#prefix' => '<div id="ldap-active-text-test-results">',
		'#suffix' => '</div>'
	);

	$form['ldap_active_text_test_submit'] = array(
		'#type' => 'button',
		'#name' => 'test',
		'#value' => t('Test'),
		'#executes_submit_callback' => FALSE,
		'#ajax' => array(
			'#wrapper' => 'ldap-active-text-test-results',
			'#callback' => 'ldap_active_text_test_form_callback'
		)
	);

	$form['ldap_active_text_test_cancel'] = array(
		'#type' => 'markup',
		'#markup' => '<input id="edit-ldap-active-text-test-cancel" class="form-submit" type="button" value="' . t('Cancel') .'" onclick="window.location.href=\'' . $config_url . '\'"/>',
	);

	if(isset($values['test'])) {
		// Calcula o resultado da query caso o formulario tenha sido submetido
		$result = ldap_active_text_parse($query_name, $values['ldap_active_text_test_parameter']);
		$form['ldap_active_text_test_results_block']['ldap_active_text_test_results']['#markup'] = print_r($result, true);
	}

	$form['#theme'] = 'system_settings_form';

	return $form;
}


/*
 * Hook form validate
 */
function ldap_active_text_config_form_validate($form, &$form_state) {
	module_load_include('inc', 'ldap_active_text', 'ldap_active_text.util');
	$values = $form_state['values'];

	if((! preg_match('/^[0-9]+$/', $values['ldap_active_text_cache_time'])) || $values['ldap_active_text_cache_time'] > 527040)
		form_set_error('ldap_active_text_cache_time', t('Invalid cache time. It must be an integer between 0 and 527040 minutes (366 days).'));

	if(! preg_match('/^[a-z0-9_]+$/', $query_name = $values['ldap_active_text_query_name']))
		form_set_error('ldap_active_text_query_name', t('Invalid query name. It must contain only lowercase letters, numbers, and underscores.'));

	if(! preg_match('/^(\{[a-z0-9_]+:[a-z0-9_]+:[a-z0-9_]+\})*\{[a-z0-9_]+:[a-z0-9_]+:[a-z0-9_,]+\}$/i', $values['ldap_active_text_query_string']))
		form_set_error('ldap_active_text_query_string', t('Invalid query string'));

	if($form['#lac_mode'] == 'add' && ldap_active_text_get_query($query_name))
		form_set_error('ldap_active_text_query_name', t('There is already an Active Text query with this name.'));
}


/*
 * Hook form submit (limpar cache)
 */
function ldap_active_text_cache_form_validate($form, $form_state) {

	cache_clear_all('*', 'cache_ldap_active_text', TRUE);

	drupal_set_message(t('All Active Text queries cache have been cleared successfully.'));
}


/*
 * Hook form submit (configurar queries)
 */
function ldap_active_text_config_form_submit($form, &$form_state) {
	module_load_include('inc', 'ldap_active_text', 'ldap_active_text.util');
	$values = $form_state['values'];
	$query_name = $values['ldap_active_text_query_name'];

	$query_config = ldap_active_text_get_query();

	switch($form['#lac_mode']) {
		case 'delete':
			unset($query_config[$query_name]);
		break;
		
		case 'add' || 'edit':
			$query_config[$query_name] = array(
				'enabled'      => $values['ldap_active_text_query_enabled'],
				'description'  => $values['ldap_active_text_query_description'],
				'query_string' => $values['ldap_active_text_query_string'],
				'fixed_input'  => $values['ldap_active_text_query_fixed_input_parameter'],
				'input_parameter' => $values['ldap_active_text_query_input_parameter'],
				'cache_time'      => $values['ldap_active_text_cache_time'],
				'display_prefix'  => $values['ldap_active_text_display_prefix'],
				'display_body'    => $values['ldap_active_text_display_body'],
				'display_suffix'  => $values['ldap_active_text_display_suffix'],
				'display_empty'   => $values['ldap_active_text_display_empty'],
				'multiple_values' => $values['ldap_active_text_display_multiple']
			);
	}

	variable_set('ldap_active_text_config', $query_config);

	// Forca o retorno aa pagina inicial da configuracao do modulo
	$form_state['redirect'] = '/admin/config/content/ldap_active_text';

	// Limpa o cache da query
	cache_clear_all('query:' . $query_name, 'cache_ldap_active_text', TRUE);

	drupal_set_message(t('The configuration options have been saved.'));
}


/* 
 * Callback Ajax do formulario de teste
 */
function ldap_active_text_test_form_callback($form, $form_state) {
	return $form['ldap_active_text_test_results_block']['ldap_active_text_test_results'];
}
